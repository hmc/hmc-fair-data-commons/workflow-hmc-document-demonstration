---
title: Collaborative Writing Workflow with GitLab
authors: Anton Pirogov, Daniel P. Mohr
---

### table of contents

[[_TOC_]]

# Collaborative Writing Workflow with GitLab

Use [this template repository](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/workflow-hmc-document-template)
as a starting point for a structured, asynchronous and FAIR document writing workflow for mid- to long-term collaboration (spanning weeks or longer).

*If you are looking for a good solution for synchronous, short-term, real-time collaboration on notes that is compatible with this workflow, consider using **HedgeDoc**, the collaborative Markdown editor. You can use e.g. [this instance](https://notes.desy.de/) hosted by DESY on behalf of HIFIS.*

* If you are interested in using this workflow for a **new document**, read the complete [Getting started](#getting-started) section.

* If you are interested in contributing to an **existing document**, read the [Usage (as document contributor)](#usage-as-document-contributor) section.

* If you want to **learn more** about how this workflow helps with implementing the 
**[FAIR principles](https://www.go-fair.org/fair-principles/)**, check out
[this document](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/workflow-hmc-document-demonstration/-/jobs/artifacts/main/raw/public/download%20collaboratively_writing_workflow_with_gitlab.pdf?job=pages).
  
  It is also created using this workflow and is developed [here](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/workflow-hmc-document-demonstration).

## Summary

The workflow used for this document is based on **standard Gitlab functionality** and is centered around having a structured approach to **discussion**, **proposal** and **agreement to changes** in the document.
The **[provenance](https://en.wikipedia.org/wiki/Provenance#Data_provenance)** of the document is therefore captured in the **[tracked history](https://git-scm.com/book/en/v2/Git-Basics-Viewing-the-Commit-History)** of the files as well as the **structured discussions** you had in the GitLab project of your document. The main advantage over other approaches is that **nothing is ever truly lost**, and you enrich your creation process by a lot of contextual information that **would never be available in the first place**, thereby adding more transparency and credibility.

## Getting started

### Usage (as document contributor)

If you are reading this as an interested contributor, then someone already has set up the collaboration workflow for you based on [this template repository](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/workflow-hmc-document-template). 

Good news! In order to participate, you just need to...

* Check out or create [issues](https://docs.gitlab.com/ee/user/project/issues/) and participate in the discussions.

An issue can be anything worthy of discussion, from a general strategic discussion on your topic, to a proposal to add/delete/modify something very specific in your document. It is also useful to track TODO items and distribute work (by e.g. assigning a specific person to it). Issues are numbered and you can refer to other issues using the hash symbol, i.e. `#23` will refer to Issue with the number 23. This is useful to establish semantic connections between issues, branches and merge requests.

* Create new [branches](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-new-branch) associated with your issues and add your proposed content to them.

A branch is like a separate named copy of your `main` document. You can work on safely on your copy, without any risk of breaking anything in the "real document". For each issue that represents individual TODO items or proposed changes you should create a branch. To create a branch associated with an issue, just use the number of the issue at the start, e.g. the branch `23-my-great-branch` will be associated with issue 23, i.e. the issue referred to by `#23` in your discussions.

On your branch, you can edit the files using the [GitLab Web Editor](https://codebase.helmholtz.cloud/help/user/project/repository/web_editor.html) or the [Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/) and you should provide meaningful commit messages describing what you did.

Your document is written using [Markdown](https://en.wikipedia.org/wiki/Markdown#Example), take a few minutes to familiarize yourself with the syntax. You can also switch between the markdown and the preview in the GitLab in most places.

* Create [merge requests](https://codebase.helmholtz.cloud/help/user/project/merge_requests/index.html) associated with your branches in order to discuss and evolve the changes.

A merge request is somewhat similar to an issue, but its purpose is to discuss **existing changes in a branch** before integrating them into the main branch (i.e. "copy") of the document. Each merge request is also numbered and can be referred to, but instead of a hash you use an exclamation mark, i.e. `!32` refers to merge request 23.

* Finally, when consensus is reached that a change (e.g. a new section, or fixing all typos) is ready, the maintainer will merge the changes into the main branch.

When an issue or merge request is closed (i.e. rejected) or the merge request is successfully merged (i.e. included in the main document), they are not gone and can still be accessed, e.g. in order to inspect the discussions. Closed issues and merge requests also can be re-opened, should you ever change your mind.

If all of this is new to you, ask your **document maintainer** (the leading person on this document) to give you a little hands-on introduction to all these features.

----

### Setting up (as document maintainer)

If you are reading this as a document maintainer, you want to know how to get a copy of the workflow started from this template.

As the document maintainer you are responsible for **setting up** the workflow and probably will also **moderate the discussions** and supervise and educate your collaborators. Therefore, **you should have read and understood** the process in the section above and be able to explain **commits, issues, branches and merge requests** sufficiently well to your members to become productive and **spread the knowledge**.

The next sections will guide you through the process of setting up a working document repository. This has to be done **only once for each separate document**.

If you need any help for the setup process or have questions about everyday use, 
[open an issue](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/workflow-hmc-document-demonstration/-/issues) or
[contact](https://mattermost.hzdr.de/hmc--community/messages/@a.pirogov) [us](https://mattermost.hzdr.de/hmc--community/messages/@daniel.mohr).

If you are interested in learning more about using GitLab and its project management features, consider taking [this course](https://www.hifis.net/events/2021/03/19/git-and-gitlab).

#### Getting a copy from this template for a new document

Pick one of the following ways to get a copy of this project:

##### Automatic import (preferred)

You can [import](https://codebase.helmholtz.cloud/help/user/project/import/repo_by_url.html) the complete project [workflow-hmc-document-template](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/workflow-hmc-document-template.git) as a new one in your space.

This means, go to the [new project page](https://codebase.helmholtz.cloud/projects/new), select **Import project**, press **Repo by URL**.
In the form you only need to paste the URL `https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/workflow-hmc-document-template.git` in the line **Git repository URL** and keep username/password empty. Apart from that, fill out the usual required information (Project name, description, etc) and press **Create project**. The new project you create will start with a copy of the template repository.

##### Fork

Using the GitLab web interface you can [fork](https://codebase.helmholtz.cloud/help/user/project/repository/forking_workflow.html) 
[this template repository](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/workflow-hmc-document-template). 

For this, just press the **Fork** button in the top right of the landing page of the repository.

To work around [a limitation in Gitlab](https://gitlab.com/gitlab-org/gitlab/-/issues/15598), you might want to remove the fork status of your copy, otherwise you won't be able to use the **Create Merge Request** button from an issue. If you prefer having this feature, go to **General -> Advanced -> Remove Fork Relationship** and confirm it by entering the name of your repository.

##### Manual copy

You can [create your project](https://codebase.helmholtz.cloud/help/user/project/working_with_projects.html#create-a-project) and extract all files from [workflow-hmc-document-template-main.zip](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/workflow-hmc-document-template/-/archive/main/workflow-hmc-document-template-main.zip) in your repository.

If you are not familiar with using git on your computer, this might be cumbersome (through the Web IDE you will need to upload the files one by one), so in this case you should prefer one of the other two options.

#### Adding badges

At this stage, you should have a GitLab project initialized from the template repository.

**NOTE: You need at least **maintainer** permissions to your repository to perform the next step.**

If you do not have them, ask the maintainers or owners of the GitLab group your copy is located in to give you the permissions.

To simplify access to the resulting output, you should add [**badges**](https://codebase.helmholtz.cloud/help/user/project/badges.html) as described below.

In your repository, go to **Settings -> General -> Badges** (and **expand** the section if it is collapsed).

##### Pipeline Status

The pipeline status badge indicates whether the translation step from source files to the outputs is currently running.
If it is running, it means that you should wait until it completes before you access the document (otherwise you will get not the most recent version).

To add the "pipeline status" badge, fill out the fields as follows and then press **Add badge**:

**Name:** Pipeline Status

**Link:** `https://codebase.helmholtz.cloud/%{project_path}/-/commits/%{default_branch}`

**Badge image URL:** `https://codebase.helmholtz.cloud/%{project_path}/badges/%{default_branch}/pipeline.svg`

##### Document PDF 

For quick access to the most recent version of your document, translated from the latest state of your `main` branch,
fill out the fields as follows and then press **Add badge**:

**Name:** document.pdf

**Link:** `https://codebase.helmholtz.cloud/%{project_path}/-/jobs/artifacts/%{default_branch}/raw/public/document.pdf?job=pages`

**Badge image URL:** `https://codebase.helmholtz.cloud/%{project_path}/-/jobs/artifacts/%{default_branch}/raw/document_pdf.svg?job=pages`

#### Final preparations

Now you should:

1. remove the example content in the `source` directory

For that you can use the Web IDE, where you can also delete multiple files and directories at once and then commit this directly to the `main` branch (in the next step we disable this possibility, as it usually is not a good practice)

2. Restrict direct pushes to the `main` branch 

Go to **Settings -> Repository -> Protected Branches** and restrict **Allowed to push** to **no one**.
This will enforce the merge request based workflow, so that you avoid inconsistent or accidental changes to your document by unreviewed commits.

#### Start working on your document

From now on, you and your contributors should only ever need to edit the markdown files located in `source` and the `CITATION.cff` file.

The `CITATION.cff` used here is a slightly customized version of the [CFF standard](https://citation-file-format.github.io/), which is a `YAML`-based format to specify authors and affiliations. The file should be self-explanatory (copy and adapt an author entry for each collaborator that shall have full coauthorship on the document).

#### In case you need to move your project

It is fairly easy to [move projects between different groups](https://about.gitlab.com/blog/2014/06/30/feature-highlight-groups/#transferring-an-existing-project-into-a-group).

Shall ever the need arise to move to a completely different Gitlab, Gitlab projects can be [exported and imported](https://docs.gitlab.com/ee/user/project/settings/import_export.html).
Remember that your provenance information in the
[gitlab project](https://codebase.helmholtz.cloud/help/user/project/index.html) includes
the
[git repository](https://codebase.helmholtz.cloud/help/user/project/repository/index.html) tracking every change to files that was done, and the
[issues](https://codebase.helmholtz.cloud/help/user/project/issues/csv_export.html) and
[merge requests](https://codebase.helmholtz.cloud/help/user/project/merge_requests/csv_export.html)
that were used for discussion and coordination of the work.

## Related repositories

If you are more technically inclined, you might also be interested in the following repositories:

  * [pandoc-latex-build](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/pandoc-latex-build) is the tool doing the "heavy lifting" and **can be used on your local computer** as well in order to build the document from the source files.
  * [hmc-document-template](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/hmc-document-template) is the HMC-specific template that can be used with the workflow and also as a **regular LaTeX template**.
  * [pandoc-latex-build-minmal-demonstration](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/pandoc-latex-build-minmal-demonstration) is a minimal demonstration, **without** HMC template.
  * [pandoc-latex-build-demonstration](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/pandoc-latex-build-demonstration) shows more of the possible features, also **without** HMC template.

This project was formerly hosted on: https://gitlab.hzdr.de/hmc/hmc-fair-data-commons/workflow-hmc-document-demonstration
