# Contributing

Thank's for your interest making a contribution.

Please be aware of the following:

  * Contributing is welcome!
  * Before you do a lot of work, please create an
    [issues](https://docs.gitlab.com/ee/user/project/issues/)
    to discuss your intention and coordinate your work with the project.
  * Provide your work via
    [merge request](https://codebase.helmholtz.cloud/help/user/project/merge_requests/index.html).
  * The projects

    * [hmc-document-template](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/hmc-document-template)
    * [pandoc-latex-build](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/pandoc-latex-build)
    * [pandoc-latex-build-demonstration](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/pandoc-latex-build-demonstration)
    * [pandoc-latex-build-minmal-demonstration](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/pandoc-latex-build-minmal-demonstration)
    * [workflow-hmc-document-demonstration](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/workflow-hmc-document-demonstration)
    * [workflow-hmc-document-template](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/workflow-hmc-document-template)

	are closely connected. So changing one of them could influence the others.
