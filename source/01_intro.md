# Collaboratively Writing and Storing Provenance

This workflow allows writing collaboratively a document and storing provenance
data in a git repository.
